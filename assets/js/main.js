$(function() {

	$('.reveal').waypoint({
		offset: '200px',
		handler: function(direction) {
			$(this).addClass('activated');

		}
	});

	$('section').waypoint({
		offset: '200px',
		handler: function(direction) {
			$(this).addClass('passed');

		}
	});

	$('section.interactive-diagram').waypoint({
		offset: '220px',
		handler: function(direction) {
				$('section.interactive-diagram figure a').each(function(index){
					setTimeout(function(){
						$('section.interactive-diagram figure a:eq('+index+')').addClass('visible');
					}, (index*500+600) );
					console.log('yep');
				});
		}
	});

	$('.lightbox a').magnificPopup({
	  type: 'image',
	  gallery:{
	    enabled:true
	  },
	  zoom: {
	    enabled: true, // By default it's false, so don't forget to enable it

	    duration: 300, // duration of the effect, in milliseconds
	    easing: 'ease-in-out', // CSS transition easing function

	    // The "opener" function should return the element from which popup will be zoomed in
	    // and to which popup will be scaled down
	    // By defailt it looks for an image tag:
	    opener: function(openerElement) {
	      // openerElement is the element on which popup was initialized, in this case its <a> tag
	      // you don't need to add "opener" option if this code matches your needs, it's defailt one.
	      return openerElement.is('img') ? openerElement : openerElement.find('img');
	    }
	  }
	});

	$('.icons li').not('.active').on('click', function(e) {

		e.preventDefault();
		var content_class = '.' + $(this).attr('id') + '-content';
		$('.icons li').removeClass('active');
		$(this).addClass('active');
		$('.icons .text').removeClass('visible');
		$(content_class).addClass('visible');
		console.log('ooo')

	});

	$('.interactive-diagram figure a').not('.active').on('click', function(e) {

		e.preventDefault();
		var content_class = '.' + $(this).attr('id') + '-content';
		$('.interactive-diagram figure a').removeClass('active');
		$(this).addClass('active');
		$('.interactive-diagram .text').removeClass('visible');
		$(content_class).addClass('visible');

	});

	$('.interactive-diagram .text').setAllToMaxHeight();
	$('.icons .text').setAllToMaxHeight();

	$( window ).resize(function() {

		$('.interactive-diagram .text').setAllToMaxHeight();
		$('.icons .text').setAllToMaxHeight();

	});

});